module unload intel
module load intel/19.0

module unload cmake
module load cmake/3.12.3
make
export SUITESPARSE=$PWD
echo $SUITESPARSE
cd metis-5.1.0/build/Linux-x86_64
SHARED_OPT="-O3 -DNDEBUG -march=native -ftz"
C_OPT="$SHARED_OPT"
CXX_OPT="$SHARED_OPT"
cmake ../../ -DCMAKE_BUILD_TYPE="Release" -DCMAKE_CXX_FLAGS_RELEASE="$CXX_OPT" -DCMAKE_C_FLAGS_RELEASE="$CXX_OPT" 
make install
cd ../../../
cd GraphBLAS/build
cmake ../ -DCMAKE_BUILD_TYPE="Release" -DCMAKE_CXX_FLAGS_RELEASE="$CXX_OPT" -DCMAKE_C_FLAGS_RELEASE="$CXX_OPT" 
make install
cd ../../
cd Mongoose/build
cmake ../ -DCMAKE_BUILD_TYPE="Release" -DCMAKE_CXX_FLAGS_RELEASE="$CXX_OPT" -DCMAKE_C_FLAGS_RELEASE="$CXX_OPT" -DSUITESPARSE_CONFIG_LIBRARY="$SUITESPARSE/SuiteSparse_config/libsuitesparseconfig.a"
make install
make 

